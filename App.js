import React from 'react';
import { View, Text, Button } from 'react-native';
import { createStackNavigator } from 'react-navigation'; // Version can be specified in package.json
import Splash from './app/screens/Splash';
import LoginPage from './app/screens/LoginPage';

const RootStack = createStackNavigator(
  {
    Splash : {screen: Splash},
    LoginPage: LoginPage,
  },
  {
    initialRouteName: 'LoginPage',
    headerMode: 'none'
  }
);

export default class App extends React.Component {
  render() {
    return <RootStack />;
  }
}
