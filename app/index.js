import { Navigation } from 'react-native-navigation';

import LoginPage from './LoginPage';
import Splash from './Splash';

// register all screens of the app (including internal ones)
export function registerScreens() {
  Navigation.registerComponent('LoginPage', () => LoginPage);
  Navigation.registerComponent('Splash', () => Splash);
}

// import React, {Component} from 'react';
// import {
//     StyleSheet,
//     View,
//     Text,
//     TouchableOpacity,
//     Linking,
// } from 'react-native'
// import SplashScreen from 'react-native-splash-screen'
//
// export default class example extends Component {
//
//     componentDidMount() {
//         SplashScreen.hide();
//     }
//
//
//     render() {
//         return (
//             <TouchableOpacity
//                 style={styles.container}
//                 onPress={(e)=> {
//                     Linking.openURL('http://www.devio.org/');
//                 }}
//             >
//                 <View >
//                     <Text style={styles.item}>
//                         SplashScreen 启动屏
//                     </Text>
//                     <Text style={styles.item}>
//                         @：http://www.devio.org/
//                     </Text>
//                     <Text style={styles.item}>
//                         GitHub:https://github.com/crazycodeboy
//                     </Text>
//                     <Text style={styles.item}>
//                         Email:crazycodeboy@gmail.com
//                     </Text>
//                 </View>
//             </TouchableOpacity>
//         )
//     }
//
// }
//
// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         backgroundColor: '#f3f2f2',
//         marginTop: 30
//     },
//     item: {
//         fontSize: 20,
//     },
//     line: {
//         flex: 1,
//         height: 0.3,
//         backgroundColor: 'darkgray',
//     },
// })
