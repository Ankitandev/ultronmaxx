/**
 * Splash created by Ankit Shukla
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Image,
  Alert,
  StyleSheet,
  Text,
  View, Button
} from 'react-native';
import SplashScreen from 'react-native-splash-screen';


export default class Splash extends React.Component {


  componentDidMount() {
    // setTimeout(() => {this.props.navigation.navigate('LoginPage');}, 5000);
    setTimeout(() => {SplashScreen.hide();}, 5000);


  }


  render() {
    return (
      <View style={styles.container}>

        <Image
          style={{width: 66, height: 58, marginBottom: 20}}
          source={{uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png'}}
          />
        <Text style = {styles.welcome}>Welcome to react-native</Text>
        <View style = {styles.footer}>
          <Text style = {styles.instructions}>Developer: Ankit Shukla</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000000',
    alignItems:'center',
    justifyContent: 'flex-end'
  },
  welcome: {
    fontSize: 16,
    color: '#ffffff',
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 300,
  },
  instructions: {
    fontSize: 10,
    textAlign: 'center',
    color: '#ffffff',
  },
  footer: {
    backgroundColor: '#000000',
  }
});
